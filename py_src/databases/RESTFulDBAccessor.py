"""
 * NRT-HARP-Data-Processor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2022 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
import json
import aiohttp
import requests
import traceback

from typing import List
from logging import Logger

from datetime import datetime

from ..datatypes.HARPRecord import HARPRecord
from ..datatypes.FlarePrediction import FlarePrediction
from ..datatypes.EruptionPrediction import EruptionPrediction
from ..datatypes.CMESpeedPrediction import CMESpeedPrediction


class RESTFulDBAccessor:
    TIME_INPUT_FORMAT = '%Y-%m-%dT%H:%M:%S'

    def __init__(self, base_uri: str, user: str, password: str, logger: Logger):
        self._base_uri = base_uri
        self._aggregator_predictions_uri = self._base_uri + 'aggregator/predictions/'
        self._aggregator_unprocesssed_uri = self._base_uri + 'aggregator/unprocessed/'
        self._flare_predictions_uri = self._base_uri + 'flarepred/predictions/'
        self._erupt_flare_predictions_uri = self._base_uri + 'eruptpred/predictions/'
        self._cme_speed_predictions_uri = self._base_uri + 'cmepred/predictions/'
        self._harp_uri = self._base_uri + 'harpdata/harps/'
        self._files_range_uri = self._base_uri + 'harpdata/files/'
        self._token_uri = self._base_uri + 'generate-auth-token/'
        self._logger = logger
        self._user = user
        self._pass = password
        self._session = None
        self._async_session = None

    def __get_session(self):
        if self._session is not None:
            return self._session

        try:
            self._session = requests.Session()
            self._session.auth = (self._user, self._pass)
            data = {"username": self._user, "password": self._pass}
            r = self._session.post(self._token_uri, json=data)
            if r.status_code == 200:
                token_json = json.loads(r.content)
                token = token_json['token']
                token_header = {'HTTP_AUTHORIZATION': 'Token ' + token}
                self._session.headers.update(token_header)
                return self._session
        except Exception as e:
            self.__reset_session()
            self._logger.error('RESTFulDBAccessor.get__session Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.get__session Traceback: %s', traceback.format_exc())
            self._session = None
        return None

    def __reset_session(self):
        try:
            if self._session is not None:
                self._session.close()
                self._session = None
        except Exception as e:
            self._logger.error('RESTFulDBAccessor.reset_session Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.reset_session Traceback: %s', traceback.format_exc())
            self._async_session = None

    async def __get_async_session(self):
        if self._async_session is not None:
            if self._async_session.closed is False and not self._async_session.loop.is_closed():
                return self._async_session
            else:
                self._async_session = None

        try:
            self._logger.info('RESTFulDBAccessor.get__async_session initializing session.')
            auth = aiohttp.BasicAuth(self._user, self._pass)
            conn = aiohttp.TCPConnector(limit=50, force_close=True)
            self._async_session = aiohttp.ClientSession(auth=auth, connector=conn)
            data = {"username": self._user, "password": self._pass}
            r = await self._async_session.post(self._token_uri, json=data)
            async with r:
                if r.status == 200:
                    token_json = await r.json()
                    token = token_json['token']
                    token_header = {'HTTP_AUTHORIZATION': 'Token ' + token}
                    self._async_session.headers.update(token_header)
                    return self._async_session
        except Exception as e:
            self._logger.error('RESTFulDBAccessor.get__async_session Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.get__async_session Traceback: %s', traceback.format_exc())
            self._async_session = None
        return None

    async def __reset_async_session(self):
        try:
            if self._async_session is not None:
                await self._async_session.close()
                self._async_session = None
        except Exception as e:
            self._logger.error('RESTFulDBAccessor.reset_async_session Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.reset_async_session Traceback: %s', traceback.format_exc())
            self._async_session = None

    async def save_all_clear_prediction_async(self, issue_time: datetime, all_clear_prob: float):
        """
        Method inserts an all clear prediction report into the aggregate_results table.

        :param issue_time: The time the prediction is issued.
        :param all_clear_prob: The predicted all clear percentage.

        :return: True if successful, false or throws an exception if not.
        """
        try:
            session = await self.__get_async_session()
            if session is None:
                return None

            pred_time = issue_time.strftime('%Y-%m-%dT%H:%M:%S')
            content_dict = {'issue_time': pred_time, 'all_clear_prob': all_clear_prob}
            async with session.post(self._aggregator_predictions_uri, json=content_dict) as r:
                if r.status == 201:
                    return True
                else:
                    return False
        except Exception as e:
            await self.__reset_async_session()
            self._logger.error('RESTFulDBAccessor.save_all_clear_prediction Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.save_all_clear_prediction Traceback: %s', traceback.format_exc())
            return False

    def forecast_candidate_ts(self) -> List[datetime]:
        """
        Method gets the unique time stamps in the database where data is available but not in the aggregate results

        :return: A list of datetime objects that meet the above specification
        """
        try:
            session = self.__get_session()
            if session is None:
                return None

            r = session.get(self._aggregator_unprocesssed_uri)
            if r.status_code == 200:
                results = []
                json_list = r.json()
                for t in json_list:
                    candidate = datetime.strptime(t['obs_time'], RESTFulDBAccessor.TIME_INPUT_FORMAT)
                    results.append(candidate)
                return results
            else:
                None
        except Exception as e:
            self.__reset_session()
            self._logger.error('RESTFulDBAccessor.save_flare_prediction Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.save_flare_prediction Traceback: %s', traceback.format_exc())
            return None

    async def get_reports_in_range_async(self, start_time: datetime, end_time: datetime) -> List[HARPRecord]:
        """
        Method gets the Active Regions that are active within the range of input timestamps.

        :param start_time: Start of the query period
        :param end_time: End of the query period
        :return: A list of HARPRecord objects that meet the above criteria
        """
        try:
            session = await self.__get_async_session()
            if session is None:
                return None

            start_time_str = start_time.strftime(RESTFulDBAccessor.TIME_INPUT_FORMAT)
            end_time_str = end_time.strftime(RESTFulDBAccessor.TIME_INPUT_FORMAT)
            qury = ["start_time={}".format(start_time_str), 'end_time={}'.format(end_time_str)]
            complete_url = "{}?{}".format(self._harp_uri, '&'.join(qury))
            async with session.get(complete_url) as r:
                if r.status == 200:
                    results = []
                    json_list = await r.json()
                    for harp in json_list:
                        candidate = HARPRecord(harp['harpnumber'],
                                               datetime.strptime(harp['starttime'],
                                                                 RESTFulDBAccessor.TIME_INPUT_FORMAT),
                                               datetime.strptime(harp['endtime'], RESTFulDBAccessor.TIME_INPUT_FORMAT))
                        results.append(candidate)
                    return results
                else:
                    None
        except Exception as e:
            await self.__reset_async_session()
            self._logger.error('RESTFulDBAccessor.report_by_ts Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.report_by_ts Traceback: %s', traceback.format_exc())
            return None

    async def get_file_count_for_harp_async(self, harp_number: int) -> int:
        """
        Gets a count of the number of files present for a harp

        :param harp_number: The HARP id of interest
        :return: The number of files in the database for this HARP
        """
        try:
            session = await self.__get_async_session()
            if session is None:
                return -1

            complete_url = "{}{}/".format(self._harp_uri, str(harp_number))
            async with session.get(complete_url) as r:
                if r.status == 200:
                    json_resp = await r.json()
                    files = json_resp['files']
                    file_count = len(files)
                    return file_count

        except Exception as e:
            await self.__reset_async_session()
            self._logger.error('RESTFulDBAccessor.get_eruptive_reports_between Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.get_eruptive_reports_between Traceback: %s', traceback.format_exc())
            return -1

    async def get_times_for_available_files_in_harp_between_async(self, harp_number: int, start_time: datetime,
                                                                  end_time: datetime) -> List[datetime]:
        """
        Method gets the descriptor of files available for the specified HARP at each time step between the input times.
        They will be placed into a dataframe with the index set to the ObsStart datetime.

        :param harp_number: The harp number to select from the database
        :param start_time: The start time for values to select from the database, all values are from a time after this
        :param end_time:  The last time for values to select from the database
        :return:  DataFrame with the values in it, or None if nothing was found
        """
        header = ['ObsStart']
        answer = []

        try:
            session = await self.__get_async_session()
            if session is None:
                return None

            qury_start = start_time.strftime('%Y-%m-%dT%H:%M:%S')
            qury_end = end_time.strftime('%Y-%m-%dT%H:%M:%S')
            qury = ["starttime={}".format(qury_start), "endtime={}".format(qury_end),
                    "harp_number={}".format(harp_number)]

            complete_url = "{}?{}".format(self._files_range_uri, '&'.join(qury))

            async with session.get(complete_url) as response:
                if response.status == 200:
                    result_json = await response.json()

                    for step in result_json:
                        obs_start_str = step['obsstart']
                        obs_start = datetime.strptime(obs_start_str, '%Y-%m-%dT%H:%M:%S')
                        bit_ok = step['bitmapdownloaded']
                        conf_ok = step['confdownloaded']
                        mag_ok = step['magnetogramdownloaded']
                        if bit_ok == 1 and conf_ok == 1 and mag_ok == 1:
                            answer.append([obs_start])

        except Exception as e:
            await self.__reset_async_session()
            self._logger.error('RESTFulDBAccessor.get_times_for_available_files_in_harp_between Failed with: %s',
                               str(e))
            self._logger.debug('RESTFulDBAccessor.get_times_for_available_files_in_harp_between Traceback: %s',
                               traceback.format_exc())
            return None

        return answer

    async def get_eruptive_reports_between_async(self, harp_number: int, start_time: datetime, end_time: datetime) -> \
            List[EruptionPrediction]:
        """
        Method gets the Eruptive Flare Reports for an Active Region in the query range

        :param harp_number: The HARP number of the active region to query for
        :param start_time: The start of the query period
        :param end_time: The end of the query period
        :return: A list of EruptionPrediction objects that meet the above criteria
        """

        try:
            session = await self.__get_async_session()
            if session is None:
                return None

            start_time = start_time.strftime(RESTFulDBAccessor.TIME_INPUT_FORMAT)
            end_time = end_time.strftime(RESTFulDBAccessor.TIME_INPUT_FORMAT)

            qury = ["starttime={}".format(start_time), "endtime={}".format(end_time),
                    'harp_number={}'.format(harp_number)]
            complete_url = "{}?{}".format(self._erupt_flare_predictions_uri, '&'.join(qury))
            async with session.get(complete_url) as r:
                if r.status == 200:
                    json_list = await r.json()
                    if len(json_list) > 0:
                        flares = []
                        for obj in json_list:
                            result = EruptionPrediction(harp_number,
                                                        datetime.strptime(obj['obs_start'],
                                                                          RESTFulDBAccessor.TIME_INPUT_FORMAT),
                                                        obj['erupt_prob'], obj['non_erupt_prob'],
                                                        obj['lat_min'], obj['lon_min'], obj['lat_max'], obj['lon_max'],
                                                        obj['crval1'], obj['crval2'], obj['crln_obs'], obj['crlt_obs'])
                            flares.append(result)
                        return flares
                    else:
                        return None
                else:
                    self._logger.debug('RESTFulDBAccessor.get_eruptive_reports_between_async wrong status: %s',
                                       r.status)
                    None
        except Exception as e:
            await self.__reset_async_session()
            self._logger.error('RESTFulDBAccessor.get_eruptive_reports_between Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.get_eruptive_reports_between Traceback: %s', traceback.format_exc())
            return None

    async def get_flare_reports_between_async(self, harp_number: int, start_time: datetime, end_time: datetime) -> List[
        FlarePrediction]:
        """
        Method gets the Flare Reports for an Active Region that are in the input time period

        :param harp_number: The HARP number of the active region to query for
        :param start_time: The start of the query period
        :param end_time: The end of the query period
        :return: A list of FlarePrediction objects that meet the above criteria
        """

        try:
            session = await self.__get_async_session()
            if session is None:
                return None

            start_time = start_time.strftime(RESTFulDBAccessor.TIME_INPUT_FORMAT)
            end_time = end_time.strftime(RESTFulDBAccessor.TIME_INPUT_FORMAT)

            qury = ["starttime={}".format(start_time), "endtime={}".format(end_time),
                    'harp_number={}'.format(harp_number)]
            complete_url = "{}?{}".format(self._flare_predictions_uri, '&'.join(qury))

            async with session.get(complete_url) as r:
                if r.status == 200:
                    json_list = await r.json()
                    if len(json_list) > 0:
                        flares = []
                        for obj in json_list:
                            result = FlarePrediction(harp_number,
                                                     datetime.strptime(obj['obs_start'],
                                                                       RESTFulDBAccessor.TIME_INPUT_FORMAT),
                                                     obj['meta_flare_prob'], obj['meta_non_flare_prob'],
                                                     obj['mod1_flare_prob'], obj['mod1_non_flare_prob'],
                                                     obj['mod2_flare_prob'],
                                                     obj['mod2_non_flare_prob'], obj['mod3_flare_prob'],
                                                     obj['mod3_non_flare_prob'], obj['lat_min'], obj['lon_min'],
                                                     obj['lat_max'],
                                                     obj['lon_max'], obj['crval1'], obj['crval2'], obj['crln_obs'],
                                                     obj['crlt_obs'])
                            flares.append(result)
                        return flares
                    else:
                        return None
                else:
                    self._logger.debug('RESTFulDBAccessor.get_flare_reports_between wrong status: %s', r.status)
                    return None
        except Exception as e:
            await self.__reset_async_session()
            self._logger.error('RESTFulDBAccessor.get_flare_reports_between Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.get_flare_reports_between Traceback: %s', traceback.format_exc())
            return None

    async def get_cme_speed_reports_between_async(self, harp_number: int, start_time: datetime, end_time: datetime) -> \
            List[CMESpeedPrediction]:
        """
        Method gets the CME Speed Reports for an Active Region betwwen start and end time.

        :param harp_number: The HRRP number of the predictions to query
        :param start_time: The start of the query period
        :param end_time: The end of the query period
        :return: A list of CMESpeedPrediction objects that meet the above criteria
        """

        try:
            session = await self.__get_async_session()
            if session is None:
                return None

            start_time = start_time.strftime(RESTFulDBAccessor.TIME_INPUT_FORMAT)
            end_time = end_time.strftime(RESTFulDBAccessor.TIME_INPUT_FORMAT)

            qury = ["starttime={}".format(start_time), "endtime={}".format(end_time),
                    'harp_number={}'.format(harp_number)]
            complete_url = "{}?{}".format(self._cme_speed_predictions_uri, '&'.join(qury))
            async with session.get(complete_url) as r:
                if r.status == 200:
                    json_list = await r.json()
                    if len(json_list) > 0:
                        cmes = []
                        for obj in json_list:
                            result = CMESpeedPrediction(harp_number, datetime.strptime(obj['obs_start'],
                                                                                       RESTFulDBAccessor.TIME_INPUT_FORMAT),
                                                        obj['x_flare_prob'], obj['m_flare_prob'],
                                                        obj['c_flare_prob'], obj['non_flare_prob'],
                                                        obj['combined_speed'],
                                                        obj['sc_23_speed'], obj['sc_24_speed'], obj['lat_min'],
                                                        obj['lon_min'],
                                                        obj['lat_max'], obj['lon_max'], obj['crval1'], obj['crval2'],
                                                        obj['crln_obs'], obj['crlt_obs'])
                            cmes.append(result)
                        return cmes
                    else:
                        return None
                else:
                    self._logger.debug('RESTFulDBAccessor.get_cme_speed_reports_between_async wrong status: %s',
                                       r.status)
                    return None
        except Exception as e:
            await self.__reset_async_session()
            self._logger.error('RESTFulDBAccessor.get_cme_speed_reports_between Failed with: %s', str(e))
            self._logger.debug('RESTFulDBAccessor.get_cme_speed_reports_between Traceback: %s', traceback.format_exc())
            return None
