"""
 * NRT-CME-Speed-Predictor, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2022 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
from datetime import datetime


class CMESpeedPrediction:

    def __init__(self, harp_number: int, obs_start: datetime, x_flare_prob: float, m_flare_prob: float,
                 c_flare_prob: float, non_flare_prob: float, combined_speed: float, sc_23_speed: float,
                 sc_24_speed: float, LAT_MIN: float, LON_MIN: float, LAT_MAX: float,
                 LON_MAX: float, CRVAL1: float, CRVAL2: float, CRLN_OBS: float, CRLT_OBS: float):
        self.HarpNumber = harp_number
        self.ObsStart = obs_start
        self.x_flare_prob = float(x_flare_prob) if x_flare_prob is not None else None
        self.m_flare_prob = float(m_flare_prob) if m_flare_prob is not None else None
        self.c_flare_prob = float(c_flare_prob) if c_flare_prob is not None else None
        self.non_flare_prob = float(non_flare_prob) if non_flare_prob is not None else None
        self.combined_speed = float(combined_speed) if combined_speed is not None else None
        self.sc_23_speed = float(sc_23_speed) if sc_23_speed is not None else None
        self.sc_24_speed = float(sc_24_speed) if sc_24_speed is not None else None
        self.LAT_MIN = float(LAT_MIN)
        self.LON_MIN = float(LON_MIN)
        self.LAT_MAX = float(LAT_MAX)
        self.LON_MAX = float(LON_MAX)
        self.CRVAL1 = float(CRVAL1)
        self.CRVAL2 = float(CRVAL2)
        self.CRLN_OBS = float(CRLN_OBS)
        self.CRLT_OBS = float(CRLT_OBS)
