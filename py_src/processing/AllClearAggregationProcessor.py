"""
 * NRT-All-Clear-Aggregator, a project at the Data Mining Lab
 * (http://dmlab.cs.gsu.edu/) of Georgia State University (http://www.gsu.edu/).
 *
 * Copyright (C) 2022 Georgia State University
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <http://www.gnu.org/licenses/>.
"""
import asyncio
import traceback
from datetime import datetime, timedelta
from logging import Logger
from typing import List

import numpy as np

from py_src.databases.RESTFulDBAccessor import RESTFulDBAccessor
from py_src.datatypes.HARPRecord import HARPRecord


class AllClearAggregationProcessor:

    def __init__(self, db_accessor: RESTFulDBAccessor, logger: Logger, active_delta: float,
                 min_active_time_hours: float,
                 solar_max: bool, solar_max_rate: float, solar_min_rate: float, batch_size: int):
        self._db_accessor = db_accessor
        self._logger = logger
        self._solar_max = solar_max
        self._solar_max_rate = solar_max_rate
        self._solar_min_rate = solar_min_rate
        self._batch_size = batch_size
        self._active_delta = timedelta(hours=active_delta)
        self._min_active_time_steps = int((min_active_time_hours * 60) / 12)
        self._step = timedelta(minutes=12)

    def run(self):
        asyncio.run(self.__run_async())

    async def __run_async(self):
        time = datetime.now()
        self._logger.info("AllClearAggregationProcessor initiated at %s", time)
        try:
            candidate_ts = self._db_accessor.forecast_candidate_ts()

            for chunk_num in range(0, int(len(candidate_ts) / self._batch_size) + 1):
                chunk_start = chunk_num * self._batch_size
                self._logger.info("Processing batch starting at: %s", str(chunk_start))
                futures = None
                if (chunk_start + self._batch_size) < len(candidate_ts):
                    futures = [self.__run_gen_async(candidate_ts[x + chunk_start]) for x in range(0, self._batch_size)]
                else:
                    futures = [self.__run_gen_async(candidate_ts[x]) for x in range(chunk_start, len(candidate_ts))]

                await asyncio.gather(*futures)

        except Exception as e:
            self._logger.error('AllClearAggregationProcessor.run Failed with: %s', str(e))
            self._logger.debug('AllClearAggregationProcessor.run Traceback: %s', traceback.format_exc())

        time = datetime.now()
        self._logger.info("AllClearAggregationProcessor completed at %s", time)

    async def __run_gen_async(self, timestamp: datetime):
        try:
            active_end = timestamp
            active_start = timestamp - self._active_delta
            harp_reports = await self._db_accessor.get_reports_in_range_async(active_start, active_end)
            # We remove the HARP reports that are very short in size as they tend to be garbage
            harp_reports = await self.__clean_harp_list_for_insufficient_data(harp_reports)

            if harp_reports is not None and len(harp_reports) > 0:
                processed_data = {}
                harp_reports_to_process = harp_reports.copy()
                for harp in harp_reports_to_process:
                    reports = await self.__get_reports_at_ts_async(harp.HarpNumber, timestamp)
                    if reports is not None:
                        flare_reports = reports[0]
                        erupt_reports = reports[1]
                        cme_reports = reports[2]
                        if len(flare_reports) > 0:
                            cent_p = (flare_reports[0].LON_MIN + flare_reports[0].LON_MAX) / 2

                            if cent_p <= -70:
                                self._logger.debug('Center point is less than -70 degrees for HARP: %s',
                                                   str(harp.HarpNumber))
                                self._logger.debug(
                                    'A random Forecast will be given to this HARP with probability: %s',
                                    self.__random_pred())
                                processed_data[harp.HarpNumber] = {'P(FL)': None, 'P(ER)': None,
                                                                   'CME_speed_est': None, 'Quality': 'Random'}
                            elif cent_p > 70:
                                self._logger.debug('Center point is greater than 70 degrees for HARP: %s',
                                                   str(harp.HarpNumber))
                                self._logger.debug('A search for a previous prediction will be conducted.')
                                local_flare_report_idx = 0
                                while cent_p > 70 and local_flare_report_idx < len(flare_reports) - 1:
                                    local_flare_report_idx += 1
                                    cent_p = (flare_reports[local_flare_report_idx].LON_MIN + flare_reports[
                                        local_flare_report_idx].LON_MAX) / 2

                                if local_flare_report_idx < len(flare_reports) and local_flare_report_idx < len(
                                        erupt_reports) and local_flare_report_idx < len(cme_reports):

                                    processed_data[harp.HarpNumber] = {
                                        'P(FL)': flare_reports[local_flare_report_idx].meta_flare_prob,
                                        'P(ER)': erupt_reports[local_flare_report_idx].erupt_prob,
                                        'CME_speed_est': cme_reports[local_flare_report_idx].combined_speed,
                                        'Quality': 'Valid'}
                                else:
                                    processed_data[harp.HarpNumber] = {'P(FL)': None, 'P(ER)': None,
                                                                       'CME_speed_est': None, 'Quality': 'Random'}
                            else:
                                processed_data[harp.HarpNumber] = {'P(FL)': flare_reports[0].meta_flare_prob,
                                                                   'P(ER)': erupt_reports[0].erupt_prob,
                                                                   'CME_speed_est': cme_reports[0].combined_speed,
                                                                   'Quality': 'Valid'}
                        else:
                            query_start = timestamp - timedelta(hours=12)
                            query_end = timestamp
                            data_times = await self._db_accessor.get_times_for_available_files_in_harp_between_async(
                                harp.HarpNumber, query_start, query_end)
                            if len(data_times) > 50:
                                self._logger.debug(
                                    'AllClearAggregationProcessor stopping HARP %s at %s because reports not processed yet.',
                                    str(harp.HarpNumber), timestamp.strftime('%Y-%m-%d %H:%M:%S'))
                                break
                            else:
                                self._logger.debug(
                                    'A random Forecast will be given to HARP %s at %s because there is insufficient data to produce a forecast.',
                                    harp.HarpNumber, timestamp.strftime('%Y-%m-%d %H:%M:%S'))
                                processed_data[harp.HarpNumber] = {'P(FL)': None, 'P(ER)': None,
                                                                   'CME_speed_est': None, 'Quality': 'Random'}
                    else:
                        query_start = timestamp - timedelta(hours=12)
                        query_end = timestamp
                        data_times = await self._db_accessor.get_times_for_available_files_in_harp_between_async(
                            harp.HarpNumber, query_start, query_end)
                        if len(data_times) > 50:
                            self._logger.debug(
                                'AllClearAggregationProcessor stopping HARP %s at %s because no reports found even though files are available.',
                                str(harp.HarpNumber), timestamp.strftime('%Y-%m-%d %H:%M:%S'))
                            break
                        else:
                            self._logger.debug(
                                'A random Forecast will be given to HARP %s at %s because there is not enough data to produce a forecast.',
                                harp.HarpNumber, timestamp.strftime('%Y-%m-%d %H:%M:%S'))
                            processed_data[harp.HarpNumber] = {'P(FL)': None, 'P(ER)': None,
                                                               'CME_speed_est': None, 'Quality': 'Random'}

                if len(processed_data.keys()) == len(harp_reports):
                    all_clear_prob = self.__get_all_clear_pred(processed_data, timestamp)
                    await self._db_accessor.save_all_clear_prediction_async(timestamp, all_clear_prob)
                else:
                    self._logger.debug(
                        'AllClearAggregationProcessor skipping %s due to missing reports for active region.',
                        timestamp.strftime('%Y-%m-%d %H:%M:%S'))
            else:

                if harp_reports is None:
                    # This should not happen as we should have HARPs in the list above, only error will cause this.
                    self._logger.debug('AllClearAggregationProcessor skipping %s due to harp_reports None.',
                                       timestamp.strftime('%Y-%m-%d %H:%M:%S'))
                else:
                    self._logger.debug('AllClearAggregationProcessor skipping %s due to empty harp_reports.',
                                       timestamp.strftime('%Y-%m-%d %H:%M:%S'))
        except Exception as e:
            self._logger.error('AllClearAggregationProcessor.__run_gen_async Failed with: %s for timestamp: %s', str(e),
                               timestamp.strftime('%Y-%m-%dT%H:%M:%S'))
            self._logger.debug('AllClearAggregationProcessor.__run_gen_async Traceback: %s', traceback.format_exc())

    async def __clean_harp_list_for_insufficient_data(self, harp_list: List[HARPRecord]) -> List[HARPRecord]:
        """
        Removes the harp records from the list that don't have enough data to produce Flare or CME speed reports.
        This is generally anything with fewer than 50 individual time stamps in the database.

        :param harp_list: The list to clean
        :return: A list with reports that don't meet the criteria removed
        """
        results = []
        for harp in harp_list:
            file_count = await self._db_accessor.get_file_count_for_harp_async(harp.HarpNumber)
            if file_count >= self._min_active_time_steps:
                results.append(harp)
            else:
                self._logger.debug('AllClearAggregationProcessor removing HARP %s because not enough data.',
                                   str(harp.HarpNumber))

        return results

    async def __get_reports_at_ts_async(self, harp_number: int, ts: datetime):
        """
        Method gets the Flare, Eruptive Flare, and CME Speed reports for the harp number at the given time step.

        :param harp_number: The harp number to get data for
        :param ts: The time step in the harp number to get data for
        :return: A tuple of flare, eruptive flare, and cme speed report in that order or None if any are None
        """
        start_time = ts - self._active_delta
        end_time = ts + self._step

        flare_reports = await self._db_accessor.get_flare_reports_between_async(harp_number, start_time, end_time)
        if flare_reports is not None:
            erupt_reports = await self._db_accessor.get_eruptive_reports_between_async(harp_number, start_time,
                                                                                       end_time)
            if erupt_reports is not None:
                cme_reports = await self._db_accessor.get_cme_speed_reports_between_async(harp_number, start_time,
                                                                                          end_time)
                if cme_reports is not None:
                    return flare_reports, erupt_reports, cme_reports
                else:
                    self._logger.debug('AllClearAggregationProcessor missing cme speed reports for HARP %s at %s.',
                                       harp_number, ts.strftime('%Y-%m-%d %H:%M:%S'))
            else:
                self._logger.debug('AllClearAggregationProcessor missing eruption reports for HARP %s at %s.',
                                   harp_number, ts.strftime('%Y-%m-%d %H:%M:%S'))
        else:
            self._logger.debug('AllClearAggregationProcessor missing flare reports for HARP %s at %s.',
                               harp_number, ts.strftime('%Y-%m-%d %H:%M:%S'))
        return None

    def __get_all_clear_pred(self, data, ts: datetime):
        all_clear_prob = 1 - self.__random_pred()
        probs = []
        for key, d in data.items():
            quality = d.get('Quality')
            if quality == 'Valid':
                p_fl = d.get('P(FL)')
                p_er = d.get('P(ER)')
                cmes_est = d.get('CME_speed_est')
                if p_fl is not None and p_er is not None and cmes_est is not None:
                    probs.append(1.0 - (
                            self.__eruptive_flare_activation(p_er) *
                            self.__flare_activation(p_fl) *
                            self.__cme_activation(cmes_est)))
                else:
                    probs.append(1.0 - self.__random_pred())
            else:
                probs.append(1.0 - self.__random_pred())

        # Get the joint probability by assuming independence
        if len(probs) > 0:
            all_clear_prob = np.prod(probs)
        self._logger.debug(
            'AllClearAggregationProcessor issued all clear percentage [%s] for %s, with the following input: %s',
            str(all_clear_prob), str(ts.strftime('%Y-%m-%d %H:%M:%S')), str(data))
        return all_clear_prob

    @staticmethod
    def __cme_activation(cme_speed: float) -> float:
        """
        Each of the individual components of the aggregate are now utilizing an activation function instead of simple
        thresholding. This is the activation function for the CME speed. It is a step function.

        :param cme_speed: The predicted speed of the CME

        :return: The activation level that results for the aggregate probability calculation.
        """
        if cme_speed < 500:
            return 0.0
        elif cme_speed <= 500 and cme_speed < 1000:
            return cme_speed / 1000.0
        else:
            return 1.0

    def __flare_activation(self, flare_prob: float):
        """
        Each of the individual components of the aggregate are now utilizing an activation function instead of a simple
        thresholding. This is the activation function for the Flare values.  It is a Leaky Rectified
        Linear Activation (LReLU) Function.

        :param flare_prob: The predicted Flare or Eruptive Flare probability value

        :return: The activation level that results for the aggregate probability calculation.
        """
        if flare_prob <= 0.5:
            return 0.4 * flare_prob
        else:
            return 1.6 * flare_prob - 0.6

    def __eruptive_flare_activation(self, flare_prob: float):
        """
        Each of the individual components of the aggregate are now utilizing an activation function instead of a simple
        thresholding. This is the activation function for the Eruptive Flare values.  It is a Leaky Rectified
        Linear Activation (LReLU) Function.

        :param flare_prob: The predicted Flare or Eruptive Flare probability value

        :return: The activation level that results for the aggregate probability calculation.
        """
        if flare_prob <= 0.6:
            return (1.0 / 3.0) * flare_prob
        else:
            return 2 * flare_prob - 1.0

    def __random_pred(self):
        # Issue a random prediction based on whether is solar maximum or not
        # solar_max is the instance defined in the config.ini
        # @SOLAR_MAX_RANDOM_ERUPTION_RATE: 0.0447
        # @SOLAR_MIN_RANDOM_ERUPTION_RATE: 0.0195

        if self._solar_max:
            random_p = self._solar_max_rate
        else:
            random_p = self._solar_min_rate
        return random_p
