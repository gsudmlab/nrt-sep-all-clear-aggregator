# NRT-CME-Speed-Predictor Test Build

If you wish to run this project independently on your local host for test purposes, you will need to setup several
things first.

1. The required libraries listed in the `requirements.txt` file, you should setup a conda environment with the required
   versions of the libraries by first:

       conda create -n aggregator numpy requests~=2.28.1 aiohttp~=3.8.3

   Then using the `aggregator` environment by

       conda activate aggregator

2. You should now be able to run the project using:

       python3 NRT-All-Clear-Aggregator.py

3. When done you should deactivate the environment:

       conda deactivate

***

## Runtime

1. To run this project, access to the RESTFul API that stores the data used by and reported by this process is
   required. In order to run the API see
   the [sep-prediction-restful-db](https://bitbucket.org/gsudmlab/sep-prediction-restful-db/) project for
   instructions on setup for and running the API.

2. This process also utilizes data produced by the
   [NRT-Flare-Predictor](https://bitbucket.org/gsudmlab/nrt-flare-predictor/) project. See that project to produce data
   accessed by this process.

3. This process also utilizes data produced by the
   [NRT-Eruption-Predictor](https://bitbucket.org/gsudmlab/nrt-eruption-predictor/) project. See that project to produce
   data accessed by this process.

4. This process also utilizes data produced by the
   [NRT-CME-Speed-Predictor](https://bitbucket.org/gsudmlab/nrt-cme-speed-predictor/) project. See that project to
   produce data accessed by this process.

***

[Return to README](./README.md)


***
***

## Acknowledgment

This work was supported in part by NASA Grant Award No. NNH14ZDA001N, NASA/SRAG Direct Contract and two NSF Grant
Awards: No. AC1443061 and AC1931555.

***

This software is distributed using the [GNU General Public License, Version 3](./LICENSE.txt)

![GPLv3](./images/gplv3-88x31.png)

***

© 2023 Dustin Kempton, Berkay Aydin, Rafal Angryk

[Data Mining Lab](https://dmlab.cs.gsu.edu/)

[Georgia State University](https://www.gsu.edu/)