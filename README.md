# NRT-All-Clear-Aggregator

* [INFO](#markdown-header-info)
* [TEST BUILD](#markdown-header-test-build)
* [CONFIGURATION](#markdown-header-configuration)

***
***

## INFO

The NRT-All-Clear-Aggregator project is one part of a larger microservice system. This process produces a probabilistic
output that represents how confident the model is that there will not be CME eruption leading to an SEP event within the
next 24 hours. This is done for each time step of data available when combining all active regions that occurred at that
time in database.

The required libraries are listed in the `requirements.txt` file, but the setup process in
the [TEST_BUILD.md](TEST_BUILD.md) file should be followed for proper operation.

## TEST Build

The build section is only for building and running on an individual basis for testing. If deploying the entire system,
go to the main deployment process utilizing `docker-compose`, see the
main [deployment project](https://bitbucket.org/gsudmlab/isepcleardeploy).

If you wish to continue to run just the local project, continue to [TEST_BUILD.md](TEST_BUILD.md)

***

## CONFIGURATION

The default config file for this process is config.ini. It is copied into the image at build time.

A full listing of the lines in the config file and what they control are listed in the [CONFIG.md](CONFIG.md)
file.

***
***

## Acknowledgment

This work was supported in part by NASA Grant Award No. NNH14ZDA001N, NASA/SRAG Direct Contract and two NSF Grant
Awards: No. AC1443061 and AC1931555.

***

This software is distributed using the [GNU General Public License, Version 3](./LICENSE.txt)

![GPLv3](./images/gplv3-88x31.png)

***

© 2023 Dustin Kempton, Berkay Aydin, Rafal Angryk

[Data Mining Lab](https://dmlab.cs.gsu.edu/)

[Georgia State University](https://www.gsu.edu/)